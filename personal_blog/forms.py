from django import forms
from django.forms import widgets
from .models import Tag, Author, Post, Comment
from django.core.exceptions import ValidationError


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Title'}),
            'author': forms.Select(attrs={'class': 'form-select'}),
            'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Some text...', 'rows': 4}),
            'tags': forms.SelectMultiple(attrs={'class': 'form-select'})
        }

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('title') == cleaned_data.get('body'):
            raise ValidationError("Post title and post body shouldn't be similar!")
        return cleaned_data


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control',
                                           'placeholder': 'Author...'})
        }


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text', 'author']
        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control', 'placeholder': ''}),
            'author': forms.TextInput(attrs={'class': 'form-control', 'placeholder': ''}),
        }


class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label='Search')
