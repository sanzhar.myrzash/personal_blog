# Generated by Django 5.0.3 on 2024-03-16 10:14

from django.db import migrations


def transfer_tags(apps, schema_editor):
    Post = apps.get_model('personal_blog.Post')
    for post in Post.objects.all():
        post.tags.set(post.tags_old.all())

def rollback_transfer(apps, schema_editor):
    Post = apps.get_model('personal_blog.Post')
    for post in Post.objects.all():
        post.tags_old.set(post.tags.all())


class Migration(migrations.Migration):

    dependencies = [
        ('personal_blog', '0006_post_tags_alter_post_tags_old'),
    ]

    operations = [
        migrations.RunPython(transfer_tags, rollback_transfer)
    ]
