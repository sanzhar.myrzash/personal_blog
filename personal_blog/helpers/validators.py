from django.core.exceptions import ValidationError

def at_least_3(string):
    if len(string) <= 3:
        raise ValidationError('This field contains at least 3 characters')