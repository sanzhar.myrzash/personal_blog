from django.urls import path
from personal_blog.views.post_views import (
    PostDetailView,
    PostCreateView,
    PostListView,
    PostUpdateView,
    PostDeleteView,
)
from personal_blog.views.comment_views import (
    CommentCreateView,
    CommentUpdateView,
    CommentDeleteView,
)

urlpatterns = [
    path('<int:pk>/detail/', PostDetailView.as_view(), name='post_detail'),
    path('create/', PostCreateView.as_view(), name='post_create'),
    path('list/', PostListView.as_view(), name='post_list'),
    path('<int:pk>/update/', PostUpdateView.as_view(), name='post_update'),
    path('<int:pk>/delete/', PostDeleteView.as_view(), name='post_delete'),

    path('<int:post_pk>/detail/comment/create/', CommentCreateView.as_view(), name='comment_create'),
    path('<int:post_pk>/detail/comment/update/', CommentUpdateView.as_view(), name='comment_update'),
    path('<int:post_pk>/detail/comment/delete/', CommentDeleteView.as_view(), name='comment_delete'),
]