from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, UpdateView, DeleteView
from ..helpers.views import CustomUpdateView

from personal_blog.forms import CommentForm
from personal_blog.helpers.views import CustomCreateView, CustomDeleteView
from personal_blog.models import Post, Comment


class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm
    template_name = 'comments/create.html'

    def form_valid(self, form):
        my_post = get_object_or_404(Post, pk=self.kwargs.get('post_pk'))
        comment = form.save(commit=False)
        comment.post = my_post
        comment.save()
        return redirect('post_detail', pk=my_post.pk)

    def get_redirect_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.post.pk})


class CommentUpdateView(UpdateView):
    model = Comment
    template_name = 'comments/update.html'
    form_class = CommentForm
    context_object_name = 'comment'
    pk_url_kwarg = 'post_pk'

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.post.pk})


# class CommentDeleteView(CustomDeleteView):
#     model = Comment
#     template_name = 'comments/delete.html'
#     key_kwarg = 'post_pk'
#     confirm_deletion = False
#
#     def get_redirect_url(self):
#         return reverse('post_detail', kwargs={'pk': self.object.post.pk})


class CommentDeleteView(DeleteView):
    model = Comment
    template_name = "comments/delete.html"
    pk_url_kwarg = 'post_pk'

    def get(self, request, *args, **kwargs):
        return self.delete( request, *args, **kwargs)

    def get_success_url(self):
        return reverse("post_detail", kwargs={'pk': self.object.post.pk})