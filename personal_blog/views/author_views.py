from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from personal_blog.forms import AuthorForm
from personal_blog.models import Author
from django.views.generic import View, ListView, CreateView, DeleteView, UpdateView


class AuthorListView(ListView):
    template_name = "authors/list.html"
    model = Author
    context_object_name = 'authors'
    ordering = ['name']
    paginate_by = 5
    paginate_orphans = 1


class AuthorCreateView(CreateView):
    template_name = "authors/create.html"
    model = Author
    form_class = AuthorForm
    success_url = reverse_lazy('author_list')


class AuthorUpdateView(UpdateView):
    model = Author
    template_name = "authors/update.html"
    form_class = AuthorForm
    context_object_name = 'author'
    success_url = reverse_lazy('author_list')


class AuthorDeleteView(DeleteView):
    model = Author
    success_url = reverse_lazy('author_list')

    def get(self, request, *args, **kwargs):
        return self.delete(request, *args, **kwargs)
