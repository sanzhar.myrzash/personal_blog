from django.shortcuts import render, redirect, get_object_or_404
from personal_blog.models import Post
from personal_blog.forms import PostForm, CommentForm, SearchForm
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.utils.http import urlencode
from django.db.models import Q
from ..helpers.views import CustomUpdateView,CustomDeleteView


class PostListView(ListView):
    template_name = 'posts/list.html'
    model = Post
    context_object_name = 'posts'
    form = SearchForm
    search_value = None

    def get(self, request, *args, **kwargs):
        self.form = self.get_search_form()
        self.search_value = self.get_search_value()
        return super().get(request, *args, **kwargs)

    def get_search_form(self):
        return self.form(self.request.GET)

    def get_search_value(self):
        if self.form.is_valid():
            return self.form.cleaned_data.get('search')
        return None

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['form'] = self.form
        if self.search_value:
            context['query'] = urlencode({'search': self.search_value})
        return context
    def get_queryset(self):
        queryset = super().get_queryset()
        if self.search_value:
            query = Q(title__icontains=self.search_value) | Q(author__name__icontains=self.search_value)
            queryset = queryset.filter(query)
        return queryset


class PostDetailView(DetailView):
    template_name = "posts/detail.html"
    model = Post

    # def get_queryset(self):
    #     return Post.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        comment_form = CommentForm()
        my_post = self.object
        comments = my_post.comments.order_by('-created_at')
        context['comments'] = comments
        context['comment_form'] = comment_form
        return context


class PostCreateView(CreateView):
    template_name = 'posts/create.html'
    model = Post
    form_class = PostForm

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.pk})


class PostUpdateView(UpdateView):
    model = Post
    template_name = "posts/update.html"
    form_class = PostForm
    context_object_name = 'post'

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.pk})


class PostDeleteView(DeleteView):
    model = Post
    template_name = "posts/delete.html"
    context_object_name = 'post'
    success_url = reverse_lazy('post_list')